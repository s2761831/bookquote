package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    private Map<Integer, Double> ISBNPrice = new HashMap<>();

    public Quoter() {
        this.ISBNPrice.put(1, 10.0);
        this.ISBNPrice.put(2, 45.0);
        this.ISBNPrice.put(3, 20.0);
        this.ISBNPrice.put(4, 35.0);
        this.ISBNPrice.put(5, 50.0);
    }

    public double getBookPrice(String isbn) {
        if (this.ISBNPrice.containsKey(Integer.valueOf(isbn))) {
            return this.ISBNPrice.get(Integer.valueOf(isbn));
        }

        return 0.0;
    }
}
